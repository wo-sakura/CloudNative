package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
)

func main() {
	http.HandleFunc("/response", response)
	http.HandleFunc("/healthz", healthz)

	err := http.ListenAndServe(":8080", nil)
	if err != nil {
	log.Fatal(err)
	}
}

func response(w http.ResponseWriter, r *http.Request)  {
	fmt.Println("response")

	//2.读取当前系统的环境变量中的 VERSION 配置，并写入 response header
	ver := os.Getenv("VERSION")
	r.Header.Add("VERSION",ver)
	for key, values := range r.Header {
		for _, i4 := range values {
			w.Header().Add(key, i4)
		}
	}
	fmt.Fprintln(w, r.Header)

	//3.Server 端记录访问日志包括客户端 IP，HTTP 返回码，输出到 server 端的标准输出
	fmt.Printf("visit from ip: %v status code: %d\n", r.Host, 200)
}

//4.当访问 localhost/healthz 时，应返回 200
func healthz(w http.ResponseWriter, r *http.Request) {
	io.WriteString(w, "200\n")
}