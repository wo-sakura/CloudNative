//构建Docker镜像 

docker build -t httpserver .



//查看构建的docker镜像IMAGE ID 

docker images



//运行httpserver 

docker run -d -p 8080:8080 --name httpserver aab21a639170



//本地测试

浏览器登录192.168.34.2:8080/healthz，返回200



//通过 nsenter 进入容器查看 IP 配置

nsenter -t 7297 -n ip a